import {createRouter, createWebHistory} from "vue-router"
import aboutRouters from './modules/about'
import serviceRouters from './modules/services'
import skillRouters from './modules/skill'
import educationRouters from './modules/education'
import experienceRouters from './modules/experience'

const routes = [
    ...aboutRouters,
    ...serviceRouters,
    ...skillRouters,
    ...educationRouters,
    ...experienceRouters,
    {
        path: '/admin/home',
        name: 'adminHome',
        component: () =>
            import(/* webpackChunkName: "adminHome" */ "../components/admin/home/index.vue"),
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/',
        name: 'Home',
        component: () =>
            import(/* webpackChunkName: "Home" */ "../components/pages/home/index.vue"),
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/:pathMatch(.*)*',
        name: 'notFound',
        component: () =>
            import(/* webpackChunkName: "notFound" */ "../components/notFound.vue"),
        meta: {
            requiresAuth: false
        }
    },
    {
        path: '/login',
        name: 'Login',
        component: () =>
            import(/* webpackChunkName: "Login" */ "../components/auth/login.vue"),
        meta: {
            requiresAuth: false
        }
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
})
router.beforeEach((to, from) => {
    if (to.meta.requiresAuth && !localStorage.getItem('token')) {
        return {name: 'Login'}
    }
    if (to.meta.requiresAuth == false && localStorage.getItem('token')) {
        return {name: 'adminHome'}
    }
});
export default router
