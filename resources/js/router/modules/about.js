export default [
    {
        name: 'adminAbout',
        path: '/admin/about',
        component: () =>
            import(/* webpackChunkName: "adminAbout" */ "../../components/admin/about/index.vue"),
        meta: {
            requiresAuth: true
        }
    },
];