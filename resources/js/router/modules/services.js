export default [
    {
        name: 'adminService',
        path: '/admin/services',
        component: () =>
            import(/* webpackChunkName: "adminService" */ "../../components/admin/services/index.vue"),
        meta: {
            requiresAuth: true
        }
    },
];
