export default [
    {
        name: 'adminEducation',
        path: '/admin/education',
        component: () =>
            import(/* webpackChunkName: "adminAbout" */ "../../components/admin/education/index.vue"),
        meta: {
            requiresAuth: true
        }
    }
]
