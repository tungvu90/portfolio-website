export default [
    {
        name: 'adminExperience',
        path: '/admin/experience',
        component: () =>
            import(/* webpackChunkName: "adminAbout" */ "../../components/admin/experiences/index.vue"),
        meta: {
            requiresAuth: true
        }
    }
]
