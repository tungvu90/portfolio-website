export default [
    {
        name: 'adminSkill',
        path: '/admin/skill',
        component: () =>
            import(/* webpackChunkName: "adminSkill" */ "../../components/admin/skills/index.vue"),
        meta: {
            requiresAuth: true
        }
    }
]
