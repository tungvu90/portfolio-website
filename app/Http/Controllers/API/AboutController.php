<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\About;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AboutController extends Controller
{
    public function edit_about()
    {
        return About::orderBy('id', 'desc')->first();
    }

    public function update_about(Request $request, $id)
    {
        $about = About::find($id);
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);
        $name = '';
        if ($request->photo != 'null' && !empty($request->photo)) {
            if ($about->photo != $request->photo) {
                $name = time() . '.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];
                $img = Image::make($request->photo)->resize(700, 500);
                $upload_path = public_path() . '/img/upload/';
                $image = $upload_path . $about->photo;
                $img->save($upload_path . $name);
                if (file_exists($image)) {
                    @unlink($image);
                }
            }
        }
        if (empty($name)) {
            $name = $about->photo;
        }
        $name_cv = '';
        if ($request->cv != 'null' && !empty($request->cv)) {
            if ($about->cv != $request->cv) {
                $name_cv = time() . '.' . explode('/', explode(':', substr($request->cv, 0, strpos($request->cv, ';')))[1])[1];
                $img = Image::make($request->cv)->resize(700, 500);
                $upload_path = public_path() . '/img/upload/';
                $image = $upload_path . $about->cv;
                $img->save($upload_path . $name_cv);
                if (file_exists($image)) {
                    @unlink($image);
                }
            }
        }
        if (empty($name_cv)) {
            $name_cv = $about->cv;
        }
        $about->name = $request->name;
        $about->email = $request->email;
        $about->phone = $request->phone;
        $about->address = $request->address;
        $about->description = $request->description;
        $about->tagline = $request->tagline;
        $about->photo = $name;
        $about->cv = $name_cv;
        $about->save();
    }
}
